import express from 'express'
import jsonBodyParser from './middleware/jsonBodyParser'
import apiRouter from './api/router'

const app = express()

app.use(jsonBodyParser)
app.use('/api', apiRouter)

export default app