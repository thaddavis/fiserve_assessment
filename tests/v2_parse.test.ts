import request from 'supertest'
import app from '../server'
import { ResponseBodyDto } from '../api/v1/parse'
import { validateRequestBodyMiddlewareResponseBody } from '../middleware/validateRequestBody'

describe('/api/v2/parse', () => {
  it('should work as expected for valid input', async () => {
    const res = await request(app)
      .post('/api/v2/parse')
      .send({
        data: 'JOHN0000MICHAEL0009994567'
      })

    expect(res.status).toBe(200)

    const responseBody: ResponseBodyDto = res.body
    expect(responseBody).toHaveProperty('statusCode')
    expect(responseBody).toHaveProperty('data')

    expect(responseBody.statusCode).toEqual(200)
    expect(responseBody.data?.firstName).toEqual('JOHN')
    expect(responseBody.data?.lastName).toEqual('MICHAEL')
    expect(responseBody.data?.clientId).toEqual('999-4567')
  })

  it('should work as expected for INVALID input', async () => {
    const res = await request(app)
      .post('/api/v2/parse')
      .send({
        data: 'JOHN0000MICHAEL009994567'
      })

    const responseBody: validateRequestBodyMiddlewareResponseBody = res.body
    expect(Array.isArray(responseBody)).toBe(true)

    expect(res.status).toBe(400)
    expect(responseBody[0].matches).toBe('data must match /^[A-Z]+(0000)[A-Z]+(000)[\\d]{7,}$/ regular expression')
  })
})