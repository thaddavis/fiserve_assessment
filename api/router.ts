import express from 'express'
import v1router from './v1'
import v2router from './v2'
const router = express.Router()

router.use('/v1', v1router)
router.use('/v2', v2router)

export default router