import express from 'express'
import parse from './parse'
const router = express.Router()

router.use(parse)

export default router