import express, { Request, Response } from 'express'
import {
  Matches,
  IsDefined,
  IsString,
  MinLength,
  MaxLength
} from "class-validator";
import { validateRequestBody } from '../../middleware/validateRequestBody';

class RequestBodyDto {
  @IsDefined()
  @IsString()
  @MinLength(1)
  @MaxLength(100)
  @Matches(RegExp(/^[A-Z]+(0000)[A-Z]+(000)[\d]{7,}$/))
  data?: string;
}

interface ResponseBodyDto {
  statusCode: number
  data: {
    firstName: string
    lastName: string
    clientId: string
  } | null
}

const router = express.Router()

router.post('/parse', validateRequestBody(RequestBodyDto), async function (req: Request, res: Response) {
  try {
    const requestBody: RequestBodyDto = res.locals.input

    const FIRST_SET_OF_ZEROES = '0000'
    const SECOND_SET_OF_ZEROES = '000'

    const indexOfFirstSetOfZeroes = requestBody.data!.indexOf(FIRST_SET_OF_ZEROES)
    const firstName = requestBody.data!.substring(0, indexOfFirstSetOfZeroes)
    const remainderOfInputData = requestBody.data!.substring(indexOfFirstSetOfZeroes + FIRST_SET_OF_ZEROES.length)
    const indexOfSecondSetOfZeroes = remainderOfInputData.indexOf(SECOND_SET_OF_ZEROES)
    const lastName = remainderOfInputData.substring(0, indexOfSecondSetOfZeroes)
    let clientId = remainderOfInputData.substring(indexOfSecondSetOfZeroes + SECOND_SET_OF_ZEROES.length)
    clientId = clientId.slice(0, 3) + '-' + clientId.slice(3)

    const responseBody: ResponseBodyDto = {
      statusCode: 200,
      data: {
        firstName,
        lastName,
        clientId
      }
    }

    res.status(200).json(responseBody)
  } catch (e) {
    const responseBody: ResponseBodyDto = {
      statusCode: 500,
      data: null
    }

    res.status(500).json(responseBody)
  }
})

export default router
